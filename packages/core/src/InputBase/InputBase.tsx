import React, { ReactNode } from "react";
import styled, { css } from "styled-components";
import warning from "warning";
import { themeGet } from "@styled-system/theme-get";
import { useForkRef, isFilled } from "../utils";
import { formControlState, useFormControl } from "../FormControl";

export interface IBBaseProps {
  fullWidth?: boolean;
  multiline?: boolean;
}
export interface IBInputProps
  extends Exclude<React.AllHTMLAttributes<HTMLInputElement>, IBBaseProps>, IBBaseProps {
  inputComponent?: keyof HTMLElementTagNameMap;
  multiline?: boolean;
  error?: boolean;
  search?: boolean;
  onBlur?: (event?: React.FocusEvent<HTMLInputElement>) => void
  onChange?: (event?: React.FormEvent<HTMLInputElement>, args?: Array<any>) => void
}
export interface InputBaseProps extends IBInputProps {
  startAdornment?: ReactNode;
  endAdornment?: ReactNode;
  inputProps?: object & {
      ref?: React.Ref<HTMLElement>;
      rows: number | undefined;
      inputRef?: React.Ref<HTMLInputElement>;
      type?: string;
  };
  inputRef?: React.Ref<HTMLDivElement>;
}

const cursor = props => ({ cursor: props.disabled ? "default" : "text" });

const baseMultiline = props => css`
  padding: ${themeGet("space.2")(props) - 2}px 0
    ${themeGet("space.2")(props) - 1}px;
`;
const inputPadding = props => ({
  padding: `${themeGet("space.2")(props) - 2}px 0 ${themeGet("space.2")(props) -
    1}px`
});

const inputMultiline = () => css`
  height: auto;
  resize: none;
  padding: 0;
`;

const inputSearch = () => css`
  /* Improve type search style. */
  -moz-appearance: textfield;
  -webkit-appearance: textfield;
`;

const fullWidthStyle = () => css`
  width: 100%;
`;
const Base = styled.div<IBBaseProps>`
  direction: ${themeGet("defaultDirection")};
  font-family: ${themeGet("font")};
  color: ${themeGet("colors.text")};
  font-size: ${themeGet("fontSizes.2")}px;
  line-height: ${themeGet("lineHeights.input")};
  box-sizing: border-box;
  display: inline-flex;
  align-items: center;
  margin-top: 16px;
  ${cursor}
  ${props => props.multiline && baseMultiline(props)}
  ${props => props.fullWidth && fullWidthStyle()}
`;

// todo: check if correct html element tag name is provided
// otherwise show error and fallback to 'input'
// todo: use "textarea" when "multiline" prop is set true
const Input = styled.input<IBInputProps>`
  direction: inherit;
  font: inherit;
  color: currentColor;
  ${inputPadding}
  border: 0;
  box-sizing: content-box !important;
  background: none;
  height: ${themeGet("lineHeights.input")};
  margin: 0;
  -webkit-tap-highlight-color: transparent;
  display: block;
  min-width: 0;
  width: 100%;
  ::placeholder {
    color: currentColor;
    opacity: 0.5;
    transition: opacity ${themeGet("duration.fast")};
  }
  &:focus {
    outline: 0;
  }
  &:invalid {
    box-shadow: none;
  }
  ::-webkit-search-decoration {
    -webkit-appearance: none;
  }

  /* todo: label shrink + placeholder visibility management */

  ${props => props.multiline && inputMultiline()}
  ${props => props.search && inputSearch()}
`;

Input.defaultProps = {
  inputComponent: "input"
};

const useEnhancedEffect = typeof window === 'undefined' ? React.useEffect : React.useLayoutEffect;

const InputBase = React.forwardRef(function(props: InputBaseProps, ref: React.Ref<HTMLDivElement>) {
  const {
    startAdornment,
    endAdornment,
    inputComponent,
    autoComplete,
    autoFocus,
    defaultValue,
    disabled,
    error,
    fullWidth = false,
    inputRef: inputRefProp,
    inputProps: inputPropsProp = {ref: undefined},
    multiline = false,
    name,
    onBlur,
    onChange,
    onClick,
    onFocus,
    onKeyDown,
    onKeyUp,
    placeholder,
    readOnly,
    required,
    rows,
    type = "text",
    value,
    as, // just so that as would not be included in other and screw with Base element props
    ...other
  } = props;
  const { current: isControlled } = React.useRef(value != null);
  // todo: fix any, if you remove any some warnings will pop
  const inputRef: any = React.useRef();
  const handleInputRefWarning = React.useCallback(instance => {
    warning(
      !instance || instance instanceof HTMLInputElement || instance.focus,
      [
        "SnapptripDS: you have provided a `inputComponent` to the input component",
        "that does not correctly handle the `inputRef` prop.",
        "Make sure the `inputRef` prop is called with a HTMLInputElement."
      ].join("\n")
    );
  }, []);
  const handleInputPropsRefProp = useForkRef(
    inputPropsProp.ref,
    handleInputRefWarning
  );
  const handleInputRefProp = useForkRef(inputRefProp, handleInputPropsRefProp);
  const handleInputRef = useForkRef(inputRef, handleInputRefProp);

  const [focused, setFocused] = React.useState(false);

  const snapptFormControl = useFormControl();
  const fcs = formControlState({
    props,
    snapptFormControl,
    states: ["disabled", "error", "required", "filled"]
  });
  fcs.focused = snapptFormControl ? snapptFormControl.focused : focused;

  // The blur won't fire when the disabled state is set on a focused input.
  // We need to book keep the focused state manually.
  React.useEffect(() => {
    if (!snapptFormControl && disabled && focused) {
      setFocused(false);
      if (onBlur) {
        onBlur();
      }
    }
  }, [snapptFormControl, disabled, focused, onBlur]);

  const checkDirty = React.useCallback(
    obj => {
      if (isFilled(obj)) {
        if (snapptFormControl && snapptFormControl.onFilled) {
          snapptFormControl.onFilled();
        }
      } else if (snapptFormControl && snapptFormControl.onEmpty) {
        snapptFormControl.onEmpty();
      }
    },
    [snapptFormControl],
  );

  useEnhancedEffect(() => {
    if (isControlled) {
      checkDirty({ value });
    }
  }, [value, checkDirty, isControlled]);

  const handleFocus = event => {
    // Fix a bug with IE 11 where the focus/blur events are triggered
    // while the input is disabled.
    if (fcs.disabled) {
      event.stopPropagation();
      return;
    }

    if (onFocus) {
      onFocus(event);
    }

    if (snapptFormControl && snapptFormControl.onFocus) {
      snapptFormControl.onFocus(event);
    } else {
      setFocused(true);
    }
  };

  const handleBlur = event => {
    if (onBlur) {
      onBlur(event);
    }

    if (snapptFormControl && snapptFormControl.onBlur) {
      snapptFormControl.onBlur(event);
    } else {
      setFocused(false);
    }
  };

  const handleChange = (event, ...args) => {
    if (!isControlled) {
      const element = event.target || inputRef.current;
      if (element == null) {
        throw new TypeError(
          'Material-UI: Expected valid input target. ' +
            'Did you use a custom `inputComponent` and forget to forward refs? '
        );
      }

      checkDirty({
        value: element.value,
      });
    }

    // Perform in the willUpdate
    if (onChange) {
      onChange(event, ...args);
    }
  };

  const handleClick = event => {
    if (inputRef.current && event.currentTarget === event.target) {
      inputRef.current.focus();
    }

    if (onClick) {
      onClick(event);
    }
  };

  let InputComponent = inputComponent;
  let inputProps = {
    ...inputPropsProp,
    ref: handleInputRef,
  };
  
  if (typeof InputComponent !== 'string') {
    inputProps = {
      // Rename ref to inputRef as we don't know the
      // provided `inputComponent` structure.
      inputRef: handleInputRef,
      type,
      ...inputProps,
      ref: null,
    };
  } else if (multiline) {
    if (rows) {
      InputComponent = 'textarea';
    } else {
      inputProps = {
        rows,
        ...inputProps,
      };
      InputComponent = 'textarea';
    }
  } else {
    inputProps = {
      type,
      ...inputProps,
    };
  }

  return (
    <Base onClick={handleClick} ref={ref} {...other}>
      {startAdornment}
      <Input
        autoComplete={autoComplete}
        autoFocus={autoFocus}
        defaultValue={defaultValue}
        disabled={disabled}
        name={name}
        onBlur={handleBlur}
        onChange={handleChange}
        onClick={handleFocus}
        onKeyDown={onKeyDown}
        onKeyUp={onKeyUp}
        placeholder={placeholder}
        readOnly={readOnly}
        required={required}
        rows={rows}
        value={value}
        inputComponent={inputComponent}
      />
      {endAdornment}
    </Base>
  );
});
export default InputBase;
