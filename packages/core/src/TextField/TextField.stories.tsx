import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { TextField } from '..';

storiesOf('TextField', module)
  .add(
    'TextField component',
    withInfo({
      inline: true,
      text:
        'Simple styled TextField component that supports a number of the styled-system props.',
    })(() => <TextField label="اوه مای" multiline rows={4}  />),
  )
  
