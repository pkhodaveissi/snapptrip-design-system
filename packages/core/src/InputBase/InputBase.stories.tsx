import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { InputBase } from '..';
// todo: remove this from stories
storiesOf('Input', module)
  .add(
    'InputBase component',
    withInfo({
      inline: true,
      text:
        'Simple styled input component that accepts a color and whether or not to show an error container.',
    })(() => <InputBase placeholder="placeholder" />),
  )
