import React from 'react';
// import ReactDOM from 'react-dom';
// import warning from 'warning';
import Input from '../Input';
import OutlinedInput from '../OutlinedInput';
import InputLabel from '../InputLabel';
import FormControl from '../FormControl';


const variantComponent = {
  standard: Input,
  outlined: OutlinedInput,
};

export const styles = {
  /* Styles applied to the root element. */
  root: {},
};

const TextField = React.forwardRef(function TextField(props: TextFieldProps, ref: React.Ref<any>) {
  const {
    autoComplete,
    autoFocus,
    children,
    defaultValue,
    error,
    FormHelperTextProps,
    fullWidth,
    helperText,
    id,
    InputLabelProps,
    inputProps,
    InputProps,
    inputRef,
    label,
    multiline,
    name,
    onBlur,
    onChange,
    onFocus,
    placeholder,
    required = false,
    rows,
    rowsMax,
    select = false,
    SelectProps,
    type,
    value,
    variant = 'standard',
    ...other
  } = props;

  // const [labelWidth, setLabelWidth] = React.useState(0);
  // const labelRef = React.useRef(null);
  // React.useEffect(() => {
  //   if (variant === 'outlined') {
  //     // #StrictMode ready
  //     const labelNode = ReactDOM.findDOMNode(labelRef.current);
  //     setLabelWidth(labelNode != null ? labelNode.offsetWidth : 0);
  //   }
  // }, [variant, required]);

  // warning(
  //   !select || Boolean(children),
  //   'Material-UI: `children` must be passed when using the `TextField` component with `select`.',
  // );

  const InputMore = {};

  // if (variant === 'outlined') {
  //   if (InputLabelProps && typeof InputLabelProps.shrink !== 'undefined') {
  //     InputMore.notched = InputLabelProps.shrink;
  //   }

  //   InputMore.labelWidth = labelWidth;
  // }

  const helperTextId = helperText && id ? `${id}-helper-text` : undefined;
  const InputComponent = variantComponent[variant];
  const InputElement = (
    <InputComponent
      aria-describedby={helperTextId}
      autoComplete={autoComplete}
      autoFocus={autoFocus}
      defaultValue={defaultValue}
      fullWidth={fullWidth}
      multiline={multiline}
      name={name}
      rows={rows}
      // rowsMax={rowsMax}
      type={type}
      value={value}
      id={id}
      // inputRef={inputRef}
      onBlur={onBlur}
      onChange={onChange}
      onFocus={onFocus}
      placeholder={placeholder}
      // inputProps={inputProps}
      {...InputMore}
      {...InputProps}
    />
  );

  return (
    <FormControl
      error={!!error}
      fullWidth={!!fullWidth}
      ref={ref}
      required={required}
      variant={variant}
      {...other}
    >
      {label && (
        <InputLabel
          htmlFor={id} 
          // ref={labelRef} {...InputLabelProps}
          >
          {label}
        </InputLabel>
      )}
      {InputElement}
      {/* {select ? (
        <Select aria-describedby={helperTextId} value={value} input={InputElement} {...SelectProps}>
          {children}
        </Select>
        ' '
      ) : (
        InputElement
      )} */}
      {/* {helperText && (
        <FormHelperText id={helperTextId} {...FormHelperTextProps}>
          {helperText}
        </FormHelperText>
        ' '
      )} */}
    </FormControl>
  );
});

interface TextFieldProps {
  /**
   * This prop helps users to fill forms faster, especially on mobile devices.
   * The name can be confusing, as it's more like an autofill.
   * You can learn more about it [following the specification](https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#autofill).
   */
  autoComplete?: string,
  /**
   * If `true`, the `input` element will be focused during the first mount.
   */
  autoFocus?: boolean,
  /**
   * @ignore
   */
  children?: React.ReactNode,
  /**
   * The default value of the `input` element.
   */
  defaultValue?: any,
  /**
   * If `true`, the `input` element will be disabled.
   */
  disabled?: boolean,
  /**
   * If `true`, the label will be displayed in an error state.
   */
  error?: boolean,
  /**
   * Properties applied to the [`FormHelperText`](/api/form-helper-text/) element.
   */
  FormHelperTextProps?: object,
  /**
   * If `true`, the input will take up the full width of its container.
   */
  fullWidth?: boolean,
  /**
   * The helper text content.
   */
  helperText?: React.ReactNode,
  /**
   * The id of the `input` element.
   * Use this prop to make `label` and `helperText` accessible for screen readers.
   */
  id?: string,
  /**
   * Properties applied to the [`InputLabel`](/api/input-label/) element.
   */
  InputLabelProps?: object,
  /**
   * Properties applied to the Input element.
   * It will be a [`FilledInput`](/api/filled-input/),
   * [`OutlinedInput`](/api/outlined-input/) or [`Input`](/api/input/)
   * component depending on the `variant` prop value.
   */
  InputProps?: object,
  /**
   * [Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Attributes) applied to the `input` element.
   */
  inputProps?: object,
  /**
   * This prop can be used to pass a ref callback to the `input` element.
   */
  inputRef?: Function | object,
  /**
   * The label content.
   */
  label?: React.ReactNode,
  /**
   * If `dense` or `normal`, will adjust vertical spacing of this and contained components.
   */
  margin?: 'none' | 'normal',
  /**
   * If `true`, a textarea element will be rendered instead of an input.
   */
  multiline?: boolean,
  /**
   * Name attribute of the `input` element.
   */
  name?: string,
  /**
   * @ignore
   */
  onBlur?: ((...args: any[]) => any),
  /**
   * Callback fired when the value is changed.
   *
   * @param {object} event The event source of the callback.
   * You can pull out the new value by accessing `event.target.value`.
   */
  onChange?: ((...args: any[]) => any),
  /**
   * @ignore
   */
  onFocus?: ((...args: any[]) => any),
  /**
   * The short hint displayed in the input before the user enters a value.
   */
  placeholder?: string,
  /**
   * If `true`, the label is displayed as required and the `input` element` will be required.
   */
  required?: boolean,
  /**
   * Number of rows to display when multiline option is set to true.
   */
  rows?: undefined | number,
  /**
   * Maximum number of rows to display when multiline option is set to true.
   */
  rowsMax?: string | number,
  /**
   * Render a [`Select`](/api/select/) element while passing the Input element to `Select` as `input` parameter.
   * If this option is set you must pass the options of the select as children.
   */
  select?: boolean,
  /**
   * Properties applied to the [`Select`](/api/select/) element.
   */
  SelectProps?: object,
  /**
   * Type of the `input` element. It should be [a valid HTML5 input type](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_%3Cinput%3E_types).
   */
  type?: string,
  /**
   * The value of the `input` element, required for a controlled component.
   */
  value?: any,
  /**
   * The variant to use.
   */
  variant?: 'standard' | 'outlined',
};

export default TextField;
