import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { IconButton } from '../';

storiesOf('IconButton', module)
  .add('default', () => (
    <IconButton onClick={action('button-click')} outlined name="Plus" />
  ))
  .add('with color', () => (
    <>
      <IconButton
        onClick={action('button-click')}
        inverted
        name="Calendar"
        colors="greenAndWhite"
        text="انتخاب تاریخ"
      />
      <IconButton
        onClick={action('button-click')}
        inverted
        name="Plus"
        colors="redAndWhite"
      />
      <IconButton
        onClick={action('button-click')}
        name="Plus"
        colors="greenAndWhite"
      />
      <IconButton
        onClick={action('button-click')}
        name="Calendar"
        colors="redAndWhite"
        text="انتخاب تاریخ"
      />
      <IconButton onClick={action('close')} name="Close" />
    </>
  ))
  .add('with size', () => (
    <IconButton
      onClick={action('button-click')}
      inverted
      name="Calendar"
      iconSize={48}
      text="Choose date"
    />
  ))
  .add('with other elements', () => (
    <IconButton
      onClick={action('button-click')}
      colors="greenAndWhite"
      inverted
      name="Filter"
      text="فیلتر"
    />
  ));
