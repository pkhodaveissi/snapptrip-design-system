import styled from "styled-components";
import {
  space,
  width,
  color,
  SpaceProps,
  WidthProps,
  ColorProps,
  textAlign,
  TextAlignProps,

} from "styled-system";
import theme from "../theme";
export interface BoxProps
  extends SpaceProps,
    WidthProps,
    ColorProps,
    TextAlignProps {}

const Box = styled.div<BoxProps>`
  ${space}
  ${width}
  ${color}
  ${textAlign}
`;

Box.displayName = "Box";

Box.defaultProps = {
  theme: theme
};

export default Box;
