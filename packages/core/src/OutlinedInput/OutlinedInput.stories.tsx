import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { OutlinedInput } from '..';

storiesOf('Input', module)
  .add(
    'OutlinedInput component',
    withInfo({
      inline: true,
      text:
        'Simple styled input component that accepts a color and whether or not to show an error container.',
    })(() => <OutlinedInput my={3} />),
  )

