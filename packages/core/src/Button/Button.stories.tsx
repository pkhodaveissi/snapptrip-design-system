import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { Button, Flex } from '../';

storiesOf('Button', module)
  .add(
    'Button component',
    withInfo({
      inline: true,
      text:
        'Use the <Button />,  <Button />, <Button />,  <OutlineButton/ > components to render a primitive button.',
    })(() => (
      <Button outlined size="large">
        پرداخت
      </Button>
    )),
  )
  .add('color', () => (
    <div>
      <Flex mb={2}>
        <Button mr={2}>پرداخت</Button>
        <Button colors="greenAndWhite" mr={2}>
          پرداخت سبز
        </Button>
        <Button colors="redAndWhite" mr={2}>
          Redپرداخت
        </Button>
      </Flex>
      <Flex mb={2}>
        <Button inverted mr={2}>
          پرداخت
        </Button>
        <Button inverted colors="greenAndWhite" mr={2}>
          پرداخت سبز
        </Button>
        <Button inverted colors="redAndWhite" mr={2}>
          Redپرداخت
        </Button>
      </Flex>
      <Flex mb={2}>
        <Button outlined mr={2}>
          Outlineپرداخت
        </Button>
        <Button outlined colors="greenAndWhite" mr={2}>
          Greenپرداخت
        </Button>
        <Button outlined colors="redAndWhite" mr={2}>
          Redپرداخت
        </Button>
      </Flex>
    </div>
  ))
  .add('sizes', () => (
    <div>
      <Button inverted size="large" mr={2}>
        بزرگ
      </Button>
      <Button inverted size="medium" mr={2}>
        متوسط
      </Button>
      <Button inverted size="small" mr={2}>
        کوچک
      </Button>
    </div>
  ))
  .add('width', () => (
    <Button inverted width={1}>
      Full Width
    </Button>
  ))
  .add('disabled', () => (
    <Button inverted disabled>
      Disabled
    </Button>
  ));
