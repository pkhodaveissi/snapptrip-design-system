import React from 'react';
import styled, { CSSObject } from 'styled-components';
import {
  textStyle,
  fontSize,
  fontWeight,
  textAlign,
  lineHeight,
  layout,
  space,
  color,
  TextStyleProps,
  FontSizeProps,
  FontWeightProps,
  TextAlignProps,
  LineHeightProps,
  LayoutProps,
  SpaceProps,
  ColorProps,
} from 'styled-system';
import { themeGet } from '@styled-system/theme-get';
import theme from '../theme';
// import { mapProps } from '../utils';

export interface TextProps
    extends TextStyleProps,
    FontSizeProps,
    FontWeightProps,
    TextAlignProps,
    LineHeightProps,
    LayoutProps,
    SpaceProps,
    ColorProps {
    caps?: boolean,
    light?: boolean,
    medium?: boolean,
    bold?: boolean,
    dir?: string | 'ltr' | 'rtl',
    component?: 'string',
    }

export const caps = props => {
  const capsStyle = {
    textTransform: 'uppercase',
    letterSpacing: themeGet('letterSpacings.caps')(props),
  } as CSSObject;
  return props.caps ? capsStyle : null;
};
export const light = props =>
  props.light ? { fontWeight: props.theme.light } : null;

export const medium = props =>
  props.medium ? { fontWeight: props.theme.medium } : null;

export const bold = props =>
  props.bold ? { fontWeight: props.theme.bold } : null;

export const dir = props => (props.dir ? { direction: props.dir } : null);
const elem = React.createElement;
const Text = styled(({component, children, ...props}) => {
  return elem(component, props, children)
})<TextProps>`
  ${textStyle}
  ${fontSize}
  ${fontWeight}
  ${textAlign}
  ${lineHeight}
  ${layout}
  ${space}
  ${color}
  ${caps}
  ${light}
  ${medium}
  ${bold}
  ${dir}
`;

Text.displayName = 'Text';


Text.defaultProps = {
  textAlign: 'right',
  dir: 'rtl',
  verticalAlign: 'middle',
  theme,
  component: 'div'
};

export default Text;
