import React from 'react'
import { configure, addDecorator } from '@storybook/react'
// todo: globalStyle
// import { injectGlobal } from 'styled-components'
import { ThemeProvider, Box, GlobalStyle } from '../packages/core/src'

addDecorator(story => (
  <ThemeProvider>
    <Box p={3}>{story()}</Box>
  </ThemeProvider>
))
const req = require.context('../packages', true, /\.stories\.(ts|js)x$/)

const load = () => {
  req.keys().forEach(key => {
    req(key)
  })
}
const withGlobal = cb => (
  <React.Fragment>
    <GlobalStyle />
    <Box p={3}>{cb()}</Box>
  </React.Fragment>
)

addDecorator(withGlobal)
configure(load, module)
