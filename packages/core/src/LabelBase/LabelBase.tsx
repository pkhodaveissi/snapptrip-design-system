import React from 'react';
import styled, { css } from "styled-components";
// import {
//   space,
//   fontSize,
//   fontWeight,
//   color,
//   SpaceProps,
//   FontSizeProps,
//   FontWeightProps,
//   ColorProps
// } from "styled-system";
import themeGet from '@styled-system/theme-get';
import {formControlState, useFormControl} from '../FormControl';
import theme from "../theme";
import Text from "../Text";


export interface LabelProps {
  component?: keyof HTMLElementTagNameMap,
  disabled?: boolean,
  error?: boolean,
  filled?: boolean,
  focused?: boolean,
  required?: boolean,
  isFormControl?: boolean,
}

export interface AsteriskProps {
  error?: boolean,
}

export interface LabelBaseProps extends LabelProps {
  children: React.ReactNode,
  htmlFor?: string,
}

const labelStyle = props => css`
  color: ${themeGet('colors.sjAsh')(props)};
  padding: 0;
  line-height: 1;
`;
const focusedStyle = props => css`
  color: ${themeGet('colors.sjCarbon')(props)};
`;
const disabledStyle = props => css`
  color: ${themeGet('colors.disabled')(props)};
`;
const errorStyle = props => css`
  color: ${themeGet('colors.error')(props)};
`;
const StyledLabel = styled(Text).attrs((props: LabelProps) => ({as: props.component}))<LabelProps>`
  ${labelStyle}
  ${props => props.focused && focusedStyle(props) }
  ${props => props.disabled && disabledStyle(props) }
  ${props => props.error && errorStyle(props) }
`;

StyledLabel.defaultProps = {
  theme
};

const Asterisk = styled.span.attrs(() => ({dangerouslySetInnerHTML : `&thinsp;*`}))<AsteriskProps>`
  color: inherit;
  ${props => props.error && errorStyle(props)}
`;

const LabelBase: React.FunctionComponent<LabelBaseProps> = (props: LabelBaseProps) => {
  const {
    children,
    component = 'label',
    ...other
  } = props;
  const snapptFormControl = useFormControl();
  const fcs = formControlState({
    props,
    snapptFormControl,
    states: ['required', 'focused', 'disabled', 'error', 'filled'],
  });

  return (
  <StyledLabel
    disabled={fcs.disabled}
    error={fcs.error}
    filled={fcs.filled}
    focused={fcs.focused}
    required={fcs.required}
    component={fcs.component}
    {...other}
  >
    {children}
    {fcs.required && (
      <Asterisk error={fcs.error} />
    ) }
  </StyledLabel>
)};

LabelBase.displayName = "LabelBase";

export default LabelBase;
