// color palette
const black = '#000';
const white = '#fff';
const sjCarbon = '#404040';
const sjAsh = '#a3a3a3';
const sjPlatingGray = '#e6e6e6';
const sjLightGray = '#f2f2f2';
const sjLightGreen = '#7ed321';
const sjGreen = '#00d170';
const sjDarkGreen = '#22a958';
const sjDarkBlue = '#001fbb';
const sjDarkRed = '#ce0024';
const sjRed = '#ff4232';
const sjYellow = '#ffd400';
const error = '#bd272d';
// aliases
const text = sjCarbon;
const disabled = sjPlatingGray;
const rateGreen = sjLightGreen;
const deepGreen = sjDarkGreen;
const danger = sjDarkRed;
const warning = sjYellow;
const safety = sjDarkBlue;
const snapptrip = sjRed;

const colors = {
  black,
  white,
  sjCarbon,
  sjAsh,
  sjPlatingGray,
  sjLightGray,
  sjLightGreen,
  sjGreen,
  sjDarkGreen,
  sjDarkBlue,
  sjDarkRed,
  sjRed,
  sjYellow,
  // aliases
  text,
  disabled,
  error,
  rateGreen,
  deepGreen,
  danger,
  warning,
  safety,
  snapptrip,
};

export default colors;
