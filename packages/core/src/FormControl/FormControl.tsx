import * as React from "react";
import styled, { css } from "styled-components";
import FormControlContext from "./FormControlContext";
import { isSnappElem, isAdornedStart, isFilled } from "../utils";
// todo: get rid of unneccessary propstype and use typescript
// todo: themeing apply styles

interface FormControlProps {
  /**
   * The contents of the form control.
   */
  children: JSX.Element[] | JSX.Element;
  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component?: keyof React.ElementType;
  /**
   * If `true`, the label, input and helper text should be displayed in a disabled state.
   */
  disabled?: boolean;
  /**
   * If `true`, the label should be displayed in an error state.
   */
  error?: boolean;
  /**
   * If `true`, the component will take up the full width of its container.
   */
  fullWidth: boolean;
  /**
   * If `dense` or `normal`, will adjust vertical spacing of this and contained components.
   */
  /**
   * If `true`, the label will indicate that the input is required.
   */
  required?: boolean;
  /**
   * The variant to use.
   */
  variant: "standard" | "outlined";
  /**
   * The variant to use.
   */
  ref: React.Ref<any>;
}
const fullWidthStyle = () => css`
  width: 100%;
`;

interface StyledElementProps {
  fullwidth: boolean;
  component: keyof React.ElementType;
}

const elem = React.createElement;

const StyledElement = styled(({ component, children, ...props }) => {
  return elem(component, props, children);
})<StyledElementProps>`
  display: inline-flex;
  flex-direction: column;
  position: relative;
  min-width: 0;
  padding: 0;
  margin: 0;
  border: 0;
  vertical-align: top;
  ${props => props.fullWidth && fullWidthStyle()}
`;
export default function FormControl(props: FormControlProps) {
  const {
    children,
    component: Component = "div",
    disabled = false,
    error = false,
    fullWidth = false,
    required = false,
    variant = "standard",
    ...other
  } = props;
  const [adornedStart] = React.useState(() => {
    // We need to iterate through the children and find the Input in order
    // to fully support server-side rendering.
    let initialAdornedStart = false;

    if (children) {
      React.Children.forEach(children, child => {
        if (!isSnappElem(child, ["Input", "Select"])) {
          return;
        }

        const input = isSnappElem(child, ["Select"])
          ? child.props.input
          : child;

        if (input && isAdornedStart(input.props)) {
          initialAdornedStart = true;
        }
      });
    }
    return initialAdornedStart;
  });

  const [filled, setFilled] = React.useState(() => {
    // We need to iterate through the children and find the Input in order
    // to fully support server-side rendering.
    let initialFilled = false;

    if (children) {
      React.Children.forEach(children, child => {
        if (!isSnappElem(child, ["Input", "Select"])) {
          return;
        }

        if (isFilled(child.props, true)) {
          initialFilled = true;
        }
      });
    }

    return initialFilled;
  });

  const [focused, setFocused] = React.useState(false);

  if (disabled && focused) {
    setFocused(false);
  }

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  const handleDirty = () => {
    if (!filled) {
      setFilled(true);
    }
  };

  const handleClean = () => {
    if (filled) {
      setFilled(false);
    }
  };

  const childContext = {
    adornedStart,
    disabled,
    error,
    filled,
    focused,
    onBlur: handleBlur,
    onEmpty: handleClean,
    onFilled: handleDirty,
    onFocus: handleFocus,
    required,
    variant
  };

  return (
    <FormControlContext.Provider value={childContext}>
      <StyledElement component={Component} fullWidth={fullWidth} {...other}>
        {children}
      </StyledElement>
    </FormControlContext.Provider>
  );
}
