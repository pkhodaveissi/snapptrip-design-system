import colors from './Colors';

const createMediaQuery = n => `@media screen and (min-width:${n})`;

const addAliases = (arr, aliases) =>
  aliases.forEach((key, i) =>
    Object.defineProperty(arr, key, {
      enumerable: false,
      get() {
        return this[i];
      },
    }),
  );

export const breakpoints = [32, 40, 48, 64].map(n => `${n}em`);

export const mediaQueries = breakpoints.map(createMediaQuery);
const direction = ['rtl', 'ltr'];
export const defaultDirection = direction[0];
const aliases = ['sm', 'md', 'lg', 'xl'];

addAliases(breakpoints, aliases);
addAliases(mediaQueries, aliases);

export const space = [0, 4, 8, 16, 32, 64, 128];

export const font = `'IRANSans','Montserrat','Helvetica Neue',Helvetica,Arial,sans-serif`;

export const fontSizes = [12, 14, 17, 20, 22, 24, 32, 40, 56, 72];

export const medium = 500;
export const bold = 700;
export const regular = 400;
export const light = 300;

// styled-system's `fontWeight` function can hook into the `fontWeights` object
export const fontWeights = {
  medium,
  bold,
  regular,
  light,
};

export const lineHeights = {
  standard: 1.5,
  display: 1.25,
  input: '1.1876em',
};

const letterSpacings = {
  normal: 'normal',
  caps: '0.025em',
};

// todo:theme colorStyles?
export const colorStyles = {
  ...colors,
  carbonAndWhite: {
    color: colors.sjCarbon,
    backgroundColor: colors.white,
  },
  redAndWhite: {
    color: colors.sjRed,
    backgroundColor: colors.white,
  },
  greenAndWhite: {
    color: colors.sjGreen,
    backgroundColor: colors.white,
  },
};
// colorStyles.info = colorStyles.textOnLightGray;
// colorStyles.success = colorStyles.whiteOnGreen;
// colorStyles.warning = colorStyles.textOnOrange;
// colorStyles.danger = colorStyles.whiteOnRed;

// styled-system's `borderRadius` function can hook into the `radii` object/array
export const radii = [0, 2, 6];
export const radius = '6px';
// todo:theme maxContainerWidth?
export const maxContainerWidth = '1280px';

// boxShadows
export const boxShadows = [
  `0 0 2px 0 rgba(0,0,0,.08),0 1px 4px 0 rgba(0,0,0,.16)`,
  `0 0 2px 0 rgba(0,0,0,.08),0 2px 8px 0 rgba(0,0,0,.16)`,
  `0 0 2px 0 rgba(0,0,0,.08),0 4px 16px 0 rgba(0,0,0,.16)`,
  `0 0 2px 0 rgba(0,0,0,.08),0 8px 32px 0 rgba(0,0,0,.16)`,
];

// animation duration
export const duration = {
  faster: `150ms`,
  fast: `200ms`,
  normal: `300ms`,
  slow: `450ms`,
  slowest: `600ms`,
};

// animation easing curves
const easeInOut = 'cubic-bezier(0.5, 0, 0.25, 1)';
const easeOut = 'cubic-bezier(0, 0, 0.25, 1)';
const easeIn = 'cubic-bezier(0.5, 0, 1, 1)';

const timingFunctions = {
  easeInOut,
  easeOut,
  easeIn,
};

// animation delay
const transitionDelays = {
  small: `60ms`,
  medium: `160ms`,
  large: `260ms`,
  xLarge: `360ms`,
};
export { colors };
const theme = {
  breakpoints,
  mediaQueries,
  direction,
  defaultDirection,
  space,
  font,
  fontSizes,
  fontWeights,
  lineHeights,
  letterSpacings,
  light,
  regular,
  medium,
  bold,
  colors,
  radii,
  radius,
  boxShadows,
  maxContainerWidth,
  duration,
  timingFunctions,
  transitionDelays,
  colorStyles,
};

export default theme;
