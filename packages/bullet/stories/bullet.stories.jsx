import React from "react";
import { storiesOf } from "@storybook/react";
import { generateBulletData } from "@nivo/generators";
import { Bullet } from "../src";

const data = [
  // generateBulletData('volume', 200, { measureCount: 2 }),
  // // generateBulletData('cost', 10000, { markerCount: 2 }),
  // // generateBulletData('revenue', 2, { float: true }),
  {
    id: "rules",
    title: void 0,
    subtitile: void 0,
    markers: [],
    measures: [],
    ranges: [
      {
        value: 65 * 60,
        isBase: true,
        isDateBased: false,
        title: "30,40,50%",
        subText: "test4"
      },
      {
        value: 60 * 60,
        isDateBased: true,
        title: "40,50,60%",
        subText: "test3"
      },
      {
        value: 12 * 60,
        isDateBased: true,
        title: "100,100,100%",
        subText: "test2"
      },
      {
        value: 0.5 * 60,
        isDateBased: false,
        title: "70, 80, 90%",
        subText: "test1"
      }
    ]
  }
];

const commonProps = {
  width: 900,
  height: 150,
  margin: { top: 10, right: 30, bottom: 50, left: 110 },
  titleOffsetX: -80,
  data,
  spacing: 80,
  animate: false
};

const stories = storiesOf("Bullet", module);

stories.add("default", () => <Bullet reverse {...commonProps} />);

stories.add("vertical", () => (
  <Bullet
    {...commonProps}
    layout="vertical"
    height={500}
    spacing={240}
    margin={{ ...commonProps.margin, top: 70 }}
    titleAlign="start"
    titleOffsetX={0}
    titleOffsetY={-15}
    titleRotation={-90}
  />
));

const CustomRange = ({
  x,
  y,
  width,
  height,
  data,
  item,
  color,
  onMouseEnter,
  onMouseMove,
  onMouseLeave
}) => {
  return item.isDateBased ? (
    <rect
      x={x}
      y={y}
      rx={10}
      ry={10}
      width={width > 33 ? width : 33}
      height={height}
      fill={color}
      onMouseEnter={onMouseEnter}
      onMouseMove={onMouseMove}
      onMouseLeave={onMouseLeave}
    />
  ) : (
    <rect
      x={x}
      y={y}
      rx={0}
      ry={0}
      width={width > 33 ? width : 33}
      height={height}
      fill={color}
      onMouseEnter={onMouseEnter}
      onMouseMove={onMouseMove}
      onMouseLeave={onMouseLeave}
    />
  );
};
const minutesToString = value => {
  let days = Math.floor(value / (24 * 60));
  let hours = (value % (24 * 60)) / 60;
  const daysString = days ? `${days} روز` : "";
  const hoursString = hours ? `${hours} ساعت` : "";
  const joinClause = daysString && hoursString ? " و " : "";
  return {
    text: daysString + joinClause + hoursString,
    days,
    hours,
  };
};

function customTooltip(range) {
  return (
    <div style={{textAlign:'right', direction: 'rtl'}}>
      <div>
        {range.v1.isBase
          ? "از لحظه صدور"
          : `از ${minutesToString(range.v1.value).text} مانده به ${
              range.v1.isDateBased ? "روز" : "لحظه"
            } پرواز`}
      </div>
      تا
      <div>
        {range.v0.value
          ? `${minutesToString(range.v0.value).text} مانده به ${
              range.v0.isDateBased ? "روز" : "لحظه"
            } پرواز`
          : "لحظه پرواز"}
      </div>
      <div>{range.v1.subText}</div>
    </div>
  );
}

const CustomTick = ([ranges, theme], tick) => {
  const rangeSet = new Set();
  ranges.map(item => {
    rangeSet.add(item.v1.value);
    rangeSet.add(item.v0.value);
  });
  const rangeSorted = [...rangeSet].sort((a, b) => a - b);
  let gStyle = { opacity: tick.opacity };
  if (tick.onClick) {
    gStyle["cursor"] = "pointer";
  }
  if (rangeSorted.indexOf(tick.value) === 0) {
    return (
      <g
        transform={`translate(${tick.x},${tick.y})`}
        {...(tick.onClick ? { onClick: e => onClick(e, tick.value) } : {})}
        style={gStyle}
      >
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          width="32"
          height="32"
          viewBox="0 0 26 32"
        >
          <title>Take Off</title>
          <path d="M5.632 20.71l-5.222-5.786c-0.077-0.077-0.051-0.23 0.077-0.256l1.587-0.538c0.102-0.026 0.205-0.026 0.307 0.026l3.405 2.125 4.301-1.587-5.504-7.040c-0.077-0.077-0.026-0.205 0.077-0.256l2.483-0.845c0.102-0.026 0.205-0.026 0.307 0.026l8.883 5.786 5.094-1.894c1.28-0.486 2.714 0.205 3.149 1.485s-0.256 2.688-1.562 3.123l-17.382 5.632zM24.218 20.403l-2.432-2.432-1.075 1.050-2.432 2.432 1.178 1.178 1.485-1.485v4.326h1.664v-4.326l0.41 0.41 1.050 1.075 1.178-1.178-1.024-1.050z"></path>
        </svg>
      </g>
    );
  }
  if (rangeSorted.indexOf(tick.value) === rangeSorted.length - 1) {
    return (
      <g
        transform={`translate(${tick.x - 30},${tick.y})`}
        {...(tick.onClick ? { onClick: e => onClick(e, tick.value) } : {})}
        style={gStyle}
      >
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          width="32"
          height="32"
          viewBox="0 0 32 32"
        >
          <title>receipt</title>
          <path d="M25.243 13.359v-2.796c0-0.854-0.699-1.553-1.553-1.553h-21.748c-0.854 0-1.553 0.699-1.553 1.553v2.796c1.32 0.155 2.408 1.243 2.408 2.641s-1.087 2.563-2.408 2.641v2.796c0 0.854 0.699 1.553 1.553 1.553h21.748c0.854 0 1.553-0.699 1.553-1.553v-2.796c-1.32-0.155-2.408-1.243-2.408-2.641s1.087-2.485 2.408-2.641zM13.515 18.563l-0.544 0.854c-0.078 0.078-0.155 0.078-0.233 0l-2.252-2.33c-0.621 0.854-1.165 1.631-1.631 2.175l0.777 1.398c0.078 0.078 0.078 0.233 0 0.388l-0.155 0.311c-0.078 0.078-0.155 0.078-0.233 0.078l-1.32-1.243c-0.388 0.233-0.621 0.388-0.854 0.233-0.155-0.078-0.233-0.388-0.233-0.854l-1.709-0.544c-0.078 0-0.155-0.155-0.078-0.233l0.155-0.311c0.078-0.078 0.155-0.155 0.311-0.155h1.631c0.233-0.699 0.621-1.553 1.010-2.485l-3.107-0.854c-0.078 0-0.155-0.155-0.078-0.233l0.466-0.854c0.078-0.078 0.155-0.155 0.311-0.155l3.495 0.078c1.553-2.485 2.641-2.796 3.029-2.563 0.466 0.233 0.699 1.398-0.699 3.961l1.786 3.029c0.155 0.078 0.155 0.233 0.155 0.311zM18.718 21.981h-2.019v-1.942h2.019v1.942zM18.718 18.641h-2.019v-1.942h2.019v1.942zM18.718 15.301h-2.019v-1.942h2.019v1.942zM18.718 11.961h-2.019v-1.942h2.019v1.942z"></path>
        </svg>
      </g>
    );
  }
  return (
    <g
      transform={`translate(${tick.x},${tick.y})`}
      {...(tick.onClick ? { onClick: e => onClick(e, tick.value) } : {})}
      style={gStyle}
    >
      <line
        x1={0}
        x2={tick.lineX}
        y1={0}
        y2={tick.lineY}
        style={theme.axis.ticks.line}
      />
      <text
        dominantBaseline={tick.textBaseline}
        textAnchor="start"
        transform={`translate(${tick.textX},${tick.textY}) rotate(-45)`}
        style={{ fontSize: "0.8rem", direction: "rtl", letterSpacing:"normal" }}
      >
        {minutesToString(tick.value).text}
      </text>
    </g>
  );
};
stories.add(
  "custom range",
  () => <Bullet {...commonProps} rangeComponent={CustomRange} reverse />,
  {
    info: {
      text: `You can customize ranges using the \`rangeComponent\` property.`
    }
  }
);

stories.add(
  "developement",
  () => (
    <Bullet
      {...commonProps}
      rangeComponent={CustomRange}
      reverse
      axisTickRender={CustomTick}
      rangeTooltipFormat={value => value}
      rangeTooltip={customTooltip}
    />
  ),
  {
    info: {
      text: `developing...`
    }
  }
);

const CustomMeasure = ({
  x,
  y,
  width,
  height,
  color,
  onMouseEnter,
  onMouseMove,
  onMouseLeave
}) => (
  <rect
    x={x + 2}
    y={y + 2}
    rx={height / 2}
    ry={height / 2}
    width={width - 4}
    height={height - 4}
    fill={color}
    onMouseEnter={onMouseEnter}
    onMouseMove={onMouseMove}
    onMouseLeave={onMouseLeave}
  />
);

stories.add(
  "custom measure",
  () => <Bullet {...commonProps} measureComponent={CustomMeasure} />,
  {
    info: {
      text: `You can customize measures using the \`measureComponent\` property.`
    }
  }
);

const CustomMarker = ({
  x,
  size,
  color,
  onMouseEnter,
  onMouseMove,
  onMouseLeave
}) => (
  <g
    transform={`translate(${x},0)`}
    onMouseEnter={onMouseEnter}
    onMouseMove={onMouseMove}
    onMouseLeave={onMouseLeave}
  >
    <line
      x1={0}
      x2={0}
      y1={0}
      y2={size}
      stroke={color}
      strokeWidth={2}
      strokeDasharray="2,3"
      fill="none"
    />
    <path d="M0 -10 L 10 0 L 0 10 L -10 0 Z" fill={color} />
    <path
      transform={`translate(0,${size})`}
      d="M0 -10 L 10 0 L 0 10 L -10 0 Z"
      fill={color}
    />
  </g>
);

stories.add(
  "custom marker",
  () => (
    <Bullet {...commonProps} markerSize={1} markerComponent={CustomMarker} />
  ),
  {
    info: {
      text: `You can customize markers using the \`markerComponent\` property.`
    }
  }
);

stories.add("custom title", () => (
  <Bullet
    {...commonProps}
    margin={{ ...commonProps.margin, left: 140 }}
    titleOffsetX={-110}
    data={data.map(d => ({
      ...d,
      title: (
        <text dy={-12}>
          <tspan
            style={{
              fill: "#000",
              fontWeight: 500,
              fontSize: "14px"
            }}
          >
            {d.id}
          </tspan>
          <tspan
            x={0}
            dy={18}
            style={{
              fill: "#999",
              fontSize: "12px"
            }}
          >
            description
          </tspan>
          <tspan
            x={0}
            dy={16}
            style={{
              fill: "#999",
              fontSize: "12px"
            }}
          >
            for {d.id}
          </tspan>
        </text>
      )
    }))}
  />
));
