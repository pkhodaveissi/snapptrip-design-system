import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '../IconButton';

const CloseButton = props => <IconButton {...props} name="Close" />;

CloseButton.defaultProps = {
  size: 24,
  title: 'close',
};

CloseButton.propTypes = {
  onClick: PropTypes.func,
  size: PropTypes.number,
  title: PropTypes.string,
};

CloseButton.displayName = 'CloseButton';

export default CloseButton;
