import { createGlobalStyle } from 'styled-components';
import reboot, {defaultRebootOptions} from 'styled-reboot';
// todo:font do something about this css import as it will result in another file in build folder
// remember that typing font faces directly into global style will result in fetching them every time
// route changes
import './font-face.css';


const rebootCss = reboot(defaultRebootOptions);
const GlobalStyle = createGlobalStyle`
  ${rebootCss}
  /* todo:font change this to default font after iransans config done */
  body {
    font-family: 'IRANSans', Helvetica, Arial, sans-serif;
  }

  /* todo:font iransans config */
  /* using font observer we can check when font loads and change font by appending class */
  /* body.fontLoaded {
    font-family: 'IRANSans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  } */

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }
`;

export default GlobalStyle;
