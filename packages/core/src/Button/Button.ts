import styled from 'styled-components';
import { width, space, colorStyle, WidthProps, SpaceProps, ColorStyleProps } from 'styled-system';
import theme from '../theme';
import { mapProps } from '../utils';

export interface ButtonProps
    extends
    WidthProps,
    SpaceProps,
    ColorStyleProps  {
      size?: string,
      disabled?: boolean,
      inverted?: boolean,
      outlined?: boolean,
      fullWidth?: boolean,
      theme?: typeof theme,
      onClick?: () => void
    } 


const size = props => {
  switch (props.size) {
    case 'icon':
      return {
        padding: '2px',
      };
    case 'small':
      return {
        fontSize: `${props.theme.fontSizes[1]}px`,
        minWidth: '98px',
        padding: '7px 12px',
        fontWeight: props.theme.regular,
      };
    case 'medium':
      return {
        fontSize: `${props.theme.fontSizes[1]}px`,
        padding: '9.5px 18px',
        minWidth: '160px',
        fontWeight: props.theme.medium,
      };
    case 'large':
      return {
        fontSize: `${props.theme.fontSizes[2]}px`,
        padding: '11.5px 53px',
        minWidth: '160px',
        fontWeight: props.theme.bold,
      };
    default:
      return {
        fontSize: `${props.theme.fontSizes[1]}px`,
        padding: '9.5px 18px',
      };
  }
};
const getColors = props => {
  const colorStyleObj = colorStyle(props);

  const outlinedStyle = {
    ...colorStyleObj,
    boxShadow: `inset 0 0 0 1.5px ${colorStyleObj.color}`,
  };
  const [color, backgroundColor] = props.inverted
    ? [colorStyleObj.backgroundColor, colorStyleObj.color]
    : [colorStyleObj.color, 'transparent'];
  const filledStyle = {
    color,
    backgroundColor,
  };
  return props.outlined ? outlinedStyle : filledStyle;
};



const Button = mapProps<ButtonProps>(({ fullWidth, ...props }) => ({
  width: fullWidth ? 1 : undefined,
  ...props,
}))(styled.button<ButtonProps>`
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  display: inline-block;
  vertical-align: middle;
  text-align: center;
  text-decoration: none;
  font-family: inherit;
  line-height: 1.5;
  cursor: pointer;
  border-radius: ${props => props.theme.radius};
  border-width: 0;
  border-style: solid;

  &:disabled {
    opacity: 0.25;
  }

  &:hover {
  }
  ${getColors}
  ${width}
  ${size}
  ${space};
`);

// Button.propTypes = {
//   size: PropTypes.oneOf(['small', 'medium', 'large']),
//   colors: PropTypes.oneOf(Object.keys(colorStyles)),
//   inverted: PropTypes.bool,
//   outlined: PropTypes.bool,
//   ...width.propTypes,
//   ...space.propTypes,
//   fullWidth: deprecatedPropType('width'),
// };

Button.defaultProps = {
  theme,
  colors: 'carbonAndWhite',
  inverted: false,
  outlined: false,
};

Button.displayName = 'Button';

export default Button;
