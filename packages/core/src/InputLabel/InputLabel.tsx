import React from 'react';
import styled, { css } from "styled-components";
// import {
//   space,
//   fontSize,
//   fontWeight,
//   color,
//   SpaceProps,
//   FontSizeProps,
//   FontWeightProps,
//   ColorProps
// } from "styled-system";
import themeGet from '@styled-system/theme-get';
import {useFormControl} from '../FormControl';
import theme from "../theme";
import { default as LabelBase, LabelBaseProps } from '../LabelBase';




export interface AsteriskProps {
  error?: boolean,
}

export interface InputLabelProps extends LabelBaseProps {
  /**
   * If `true`, the label is shrunk.
   */
  shrink?: boolean,
  /**
   * The variant to use.
   */
  variant?: 'standard' | 'outlined',
}

const inputLabelStyle = (props) => css`
  display: block;
  transform-origin: top ${themeGet('defaultDirection')(props) === 'rtl' ? 'right' : 'left'};
  transition: color ${themeGet('duration.fast')(props)} ${themeGet('timingFunctions.easeOut')(props)}, transform ${themeGet('duration.fast')(props)} ${themeGet('timingFunctions.easeOut')(props)};
`;
const formControlStyle = (props) => css`
  position: absolute;
  ${themeGet('defaultDirection')(props) === 'rtl' ? 'right' : 'left'}: 0;
  top: 0;
  /* slight alteration to spec spacing to match visual spec result */
  transform: translate(0, 24px) scale(1);
`;
const shrinkStyle = (props) => css`
  transform: translate(0, 1.5px) scale(0.75);
  transform-origin: top ${themeGet('defaultDirection')(props) === 'rtl' ? 'right' : 'left'};
`;

const StyledLabel = styled(LabelBase).attrs((props: InputLabelProps) => ({as: props.component}))<InputLabelProps>`
  ${inputLabelStyle}
  ${props => props.isFormControl && formControlStyle(props)}
  ${props => props.shrink && shrinkStyle(props)}
  /* maybe outlined state styles */
`;

StyledLabel.defaultProps = {
  theme
};


const InputLabel: React.FunctionComponent<InputLabelProps> = (props: InputLabelProps) => {
  const {
    shrink: shrinkProp,
    variant,
    ...other
  } = props;
  const snapptFormControl = useFormControl();
  // const fcs = formControlState({
  //   props,
  //   snapptFormControl,
  //   states: ['variant'],
  // });
  let shrink = shrinkProp;
  if (typeof shrink === 'undefined' && snapptFormControl) {
    shrink = snapptFormControl.filled || snapptFormControl.focused || snapptFormControl.adornedStart;
  }
  return (
  <StyledLabel
    isFormControl={snapptFormControl}
    shrink={shrink}
    {...other}
  />
)};

InputLabel.displayName = "InputLabel";

export default InputLabel;
