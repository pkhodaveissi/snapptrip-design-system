import styled, { CSSObject } from 'styled-components';
import { space, SpaceProps } from 'styled-system';
import { themeGet } from '@styled-system/theme-get';
import InputBase, {InputBaseProps} from '../InputBase';
import defaultTheme from '../theme';

export interface OutlinedInputProps
  extends SpaceProps, InputBaseProps {
  color?: string;
  theme: typeof defaultTheme;
  error?: boolean,
  disabled?: boolean,
}

const formControl = () => {

  return {
    'label + &': {
      marginTop: 16,
    },
  } as CSSObject;
};

const outline = (props: OutlinedInputProps) => {
  const {color} = props;
  const borderColor = color ? themeGet(`colors.${color}`)(props) : themeGet('colors.sjAsh')(props);
  const focusColor = color ? borderColor : themeGet('colors.sjCarbon')(props);
  return {
    borderColor,
    '&:hover': {
      borderColor: focusColor,
    },
    // Reset on touch devices, it doesn't add specificity
    '@media (hover: none)': {
      '&:hover': {
        borderColor: focusColor,
      },
    },
    '&:focus-within': {
      borderWidth: 2,
      borderColor: focusColor
      // transform: 'scaleX(1)',
    },
    
    //  todo: errpr state, disabled state
    //  todo: 
    
  } as CSSObject;
}

const transition = (props: OutlinedInputProps) => {
  const align = themeGet('defaultDirection')(props) === 'rtl' ? 'right' : 'left';
  return {
    transition: `padding-${align} ${themeGet('duration.fast')(props)} ${themeGet('timingFunctions.easeOut')(props)}, border-color ${themeGet('duration.fast')(props)} ${themeGet('timingFunctions.easeOut')(props)}, border-width ${themeGet('duration.fast')(props)} ${themeGet('timingFunctions.easeOut')(props)}` 
  } as CSSObject;
};

const OutlinedInput = styled(InputBase)<OutlinedInputProps>`
  position: relative;
  border-radius: ${themeGet('radius')};
  border-style: solid;
  border-width: 1px;
  min-height: 56px;
  padding: 10px 12px;
  ${transition}
  ${formControl}
  ${outline}
  ${space}
`;

OutlinedInput.displayName = 'OutlinedInput';


OutlinedInput.defaultProps = {
  theme: defaultTheme,
};

export default OutlinedInput;
