import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { Box, Input, LabelBase, Icon, InputAdornment } from '..';

storiesOf('Input', module)
  .add(
    'Input component',
    withInfo({
      inline: true,
      text:
        'Simple styled input component that accepts a color and whether or not to show an error container.',
    })(() => <Input my={3} />),
  )
  .add('Colors', () => (
    <Box width={400}>
      <Input mb={3} id="input-colors-1" placeholder="No color" />
      <Input mb={3} id="input-colors-2" color="sjDarkRed" placeholder="Red" />
      <Input mb={3} id="input-colors-3" color="sjDarkGreen" placeholder="Green" />
      <Input mb={3} id="input-colors-4" color="sjRed" placeholder="Orange" />
      <Input mb={3} id="input-colors-5" color="sjDarkBlue" placeholder="Blue" />
    </Box>
  ))
  .add('With external label', () => (
    <Box width={400}>
      <LabelBase htmlFor="sample-input">
        LabelBase!
      </LabelBase>
      <Input id="sample-input" placeholder="Click the label" />
    </Box>
  ))
  .add('With Adornments', () => (
    <Input id="sample-input" placeholder="هولدر" 
      endAdornment={<InputAdornment><Icon name="FormDot" size={20} color="sjAsh" /></InputAdornment>}
    />
  ));
