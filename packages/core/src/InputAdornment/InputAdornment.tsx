import React from "react";
import styled from "styled-components";
// import { space, SpaceProps } from 'styled-system';
import { themeGet } from "@styled-system/theme-get";
// import defaultTheme from '../theme';
import { Text } from "..";

export interface InputAdornmentBaseProps {
  inputComponent?: keyof HTMLElementTagNameMap;
}
export interface InputAdornmentProps extends InputAdornmentBaseProps {
  disableTypography?: boolean;
  children: React.ReactNode;
  position?: "start" | "end";
  disablePointerEvents?: boolean,
}
export const disablePointer = props =>
  props.disablePointerEvents ? { fontWeight: props.theme.light } : null;
const positionAdorn = props => {
  const [left, right] =
    themeGet('defaultDirection')(props) === "ltr"
      ? ["Left", "Right"]
      : ["Right", "Left"];
  const marginLeft = { [`margin${left}`]: themeGet('space.2')(props) };
  const marginRight = { [`margin${right}`]: themeGet('space.2')(props) };
  return props.position === "start" ? marginRight : marginLeft;
};

const InputAdornmentBase = styled.div.attrs(
  (props: InputAdornmentBaseProps) => ({
    as: props.inputComponent
  })
)<InputAdornmentBaseProps>`
  display: flex;
  height: 0.01em;
  max-height: 2em;
  align-items: center;
  ${positionAdorn}
  ${disablePointer}
`;

InputAdornmentBase.defaultProps = {
  inputComponent: "div"
};

const InputAdornment = function(props: InputAdornmentProps) {
  const { children, disableTypography, ...other } = props;
  return (
    <InputAdornmentBase {...other}>
      {typeof children === "string" && !disableTypography ? (
        <Text color="sjAsh">{children}</Text>
      ) : (
        children
      )}
    </InputAdornmentBase>
  );
};

InputAdornment.defaultProps = {
  disableTypography: false,
  position: "end",
  disablePointerEvents: false,
};

export default InputAdornment;
