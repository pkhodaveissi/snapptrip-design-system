const resolve = require('rollup-plugin-node-resolve')
const babel = require('rollup-plugin-babel')
const commonjs = require('rollup-plugin-commonjs')
const json = require('rollup-plugin-json')
const fileSize = require('rollup-plugin-filesize')
const css = require('rollup-plugin-css-only')
const typescript = require('rollup-plugin-typescript2')

module.exports = {
  input: 'src/index.ts',
  output: [
    {
      file: 'dist/index.cjs.js',
      format: 'cjs'
    },
    {
      file: 'dist/index.esm.js',
      format: 'esm'
    }
  ],
  plugins: [
    typescript({
      typescript: require('typescript')
    }),
    babel({
      babelrc: false,
      runtimeHelpers: true,
      plugins: [
        '@babel/plugin-transform-async-to-generator',
        ['@babel/plugin-transform-runtime', { useESModules: true }]
      ],
      presets: ['@babel/preset-env'],
      exclude: 'node_modules/**'
    }),
    commonjs(),
    json(),
    resolve(),
    css({ output: 'dist/bundle.css' }),
    fileSize()
  ],
  external: [
    'styled-components',
    'react',
    'react-dom',
    'prop-types',
    'styled-system',
    'snappjek-icons'
  ]
}
