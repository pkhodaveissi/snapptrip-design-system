import styled from 'styled-components';
import theme from '../theme';
import Text, {TextProps} from '../Text';

const Truncate = styled(Text)<TextProps>`
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;
Truncate.defaultProps = {
  theme,
};

Truncate.displayName = 'Truncate';

export default Truncate;
