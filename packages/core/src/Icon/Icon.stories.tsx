import React from 'react';
import { storiesOf } from '@storybook/react';
import * as icons from '@snappt-design-system/icons/lib';
import { Box, Flex, Icon, Truncate } from '../';

const keys = Object.keys(icons);

storiesOf('Icon', module)
  .add('Icons', () => (
    <Box p={4} color="sjCarbon" bg="white">
      <Flex wrap>
        {keys.map(name => (
          <Box key={name} width={[1 / 3, 1 / 5, 1 / 6, 1 / 8]} mx={2} my={3}>
            <Icon name={name} size={28} />
            <Truncate dir="ltr" textAlign="left" fontSize={0}>{name}</Truncate>
          </Box>
        ))}
      </Flex>
    </Box>
  ))
  .add('Color', () => (
    <div>
      <Icon color="safety" size={48} m={2} name="BedExtra" />
      <Icon color="snapptrip" size={48} m={2} name="Calendar" />
      <Icon color="warning" size={48} m={2} name="EmojiHappy" />
    </div>
  ));
