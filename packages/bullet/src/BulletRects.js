/*
 * This file is part of the nivo project.
 *
 * Copyright 2016-present, Raphaël Benitte.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import partial from 'lodash/partial'
import { TransitionMotion, spring } from 'react-motion'
import compose from 'recompose/compose'
import withPropsOnChange from 'recompose/withPropsOnChange'
import pure from 'recompose/pure'
import { motionPropTypes } from '@nivo/core'
import { interpolateColor, getInterpolatedColor } from '@nivo/colors'
import { computeRects, rgbToHex } from './compute'
import invert from 'invert-color'
class BulletRects extends Component {
  static propTypes = {
    scale: PropTypes.func.isRequired,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        v0: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
          .isRequired,
        v1: PropTypes.oneOfType([PropTypes.number, PropTypes.object]).isRequired
      })
    ).isRequired,
    layout: PropTypes.oneOf(['horizontal', 'vertical']).isRequired,
    reverse: PropTypes.bool.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    rects: PropTypes.arrayOf(
      PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired,
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        data: PropTypes.shape({
          index: PropTypes.number.isRequired,
          v0: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
            .isRequired,
          v1: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
            .isRequired,
          color: PropTypes.string.isRequired
        }).isRequired
      })
    ).isRequired,
    component: PropTypes.func.isRequired,
    onMouseEnter: PropTypes.func.isRequired,
    onMouseLeave: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    ...motionPropTypes
  }

  handleMouseEnter = (data, event) => {
    this.props.onMouseEnter(data, event)
  }

  handleMouseLeave = (data, event) => {
    this.props.onMouseLeave(data, event)
  }

  handleClick = (data, event) => {
    this.props.onClick(data, event)
  }

  render() {
    const {
      rects,
      layout,
      y,
      component,
      animate,
      motionStiffness,
      motionDamping
    } = this.props

    const transform = `translate(${layout === 'horizontal' ? 0 : y},${
      layout === 'horizontal' ? y : 0
    })`
    if (animate !== true) {
      return (
        <g transform={transform}>
          {rects.map(rect => {
            const RangeRect = React.createElement(component, {
              index: rect.data.index,
              color: rect.data.color,
              item: rect.data.v1,
              ...rect,
              onMouseEnter: partial(this.handleMouseEnter, rect.data),
              onMouseMove: partial(this.handleMouseEnter, rect.data),
              onMouseLeave: partial(this.handleMouseLeave, rect.data),
              onClick: partial(this.handleClick, rect.data)
            })

            const textWidth = rect.width > 33 ? rect.width : 33
            return (
              <Fragment key={rect.data.index}>
                {RangeRect}
                {rect.data.v1.title ? (
                  <>
                    <path
                      id={`textpath-${rect.data.index}`}
                      d={`M ${rect.x + 5},${rect.y + 15} L ${rect.x +
                        textWidth},${rect.y + 15}`}
                    />
                    <text
                      fill={invert(rgbToHex(rect.data.color), {
                        black: '#3a3a3a',
                        white: '#fafafa',
                        threshold: 0.2
                      })}
                      style={{ pointerEvents: 'none' }}
                    >
                      <textPath href={`#textpath-${rect.data.index}`}>
                        {rect.data.v1.title}
                      </textPath>
                    </text>
                  </>
                ) : null}
              </Fragment>
            )
          })}
        </g>
      )
    }

    const springConfig = {
      damping: motionDamping,
      stiffness: motionStiffness
    }
    return (
      <g transform={transform}>
        <TransitionMotion
          styles={rects.map(rect => ({
            key: `${rect.data.index}`,
            data: rect.data,
            style: {
              x: spring(rect.x, springConfig),
              y: spring(rect.y, springConfig),
              width: spring(rect.width, springConfig),
              height: spring(rect.height, springConfig),
              ...interpolateColor(rect.data.color, springConfig)
            }
          }))}
        >
          {interpolatedStyles => (
            <Fragment>
              {interpolatedStyles.map(({ key, style, data }) => {
                const color = getInterpolatedColor(style)
                const RangeRect = React.createElement(component, {
                  key: key,
                  index: Number(key),
                  data,
                  item: data.v1,
                  x: style.x,
                  y: style.y,
                  width: Math.max(style.width, 0),
                  height: Math.max(style.height, 0),
                  color,
                  onMouseEnter: partial(this.handleMouseEnter, data),
                  onMouseMove: partial(this.handleMouseEnter, data),
                  onMouseLeave: partial(this.handleMouseLeave, data),
                  onClick: partial(this.handleClick, data)
                })

                return (
                  <Fragment key={key}>
                    {RangeRect}
                    {data.v1.title ? (
                      <text
                        fill={invert(rgbToHex(color), {
                          black: '#3a3a3a',
                          white: '#fafafa',
                          threshold: 0.2
                        })}
                        x={style.x + 5}
                        y={style.y + 15}
                        style={{ pointerEvents: 'none' }}
                      >
                        {data.v1.title}
                      </text>
                    ) : null}
                  </Fragment>
                )
              })}
            </Fragment>
          )}
        </TransitionMotion>
      </g>
    )
  }
}

const EnhancedBulletRects = compose(
  withPropsOnChange(
    ['data', 'layout', 'reverse', 'scale', 'height'],
    ({ data, layout, reverse, scale, height }) => ({
      rects: computeRects({
        data,
        layout,
        reverse,
        scale,
        height
      })
    })
  ),
  pure
)(BulletRects)

EnhancedBulletRects.displayName = 'BulletRects'

export default EnhancedBulletRects
