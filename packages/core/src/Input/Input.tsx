import React from 'react';
import styled, { CSSObject } from 'styled-components';
import { space, SpaceProps } from 'styled-system';
import { themeGet } from '@styled-system/theme-get';
import InputBase, {InputBaseProps} from '../InputBase';
import defaultTheme from '../theme';

export interface InputProps
  extends SpaceProps, InputBaseProps {
  color?: string;
  theme?: typeof defaultTheme;
  disableUnderline?: boolean,

}

const formControl = () => {

  return {
    'label + &': {
      marginTop: 16,
    },
  } as CSSObject;
};

const underline = (props: InputProps) => {
  return {
    '&:after': {
      borderBottom: `2px solid ${themeGet('colors.snapptrip')(props)}`,
      left: 0,
      bottom: 0,
      // Doing the other way around crash on IE 11 "''" https://github.com/cssinjs/jss/issues/242
      content: '""',
      position: 'absolute',
      right: 0,
      transform: props.error ? 'scaleX(1)' :'scaleX(0)',
      transition: `transform ${themeGet('duration.fast')(props)} ${themeGet('timingFunctions.easeOut')(props)}`,
      pointerEvents: 'none', // Transparent to the hover style.
      // todo: themeing: make this(transform origin) adaptable to locally set direction via props
      transformOrigin: themeGet('defaultDirection')(props) ==='rtl' ? 'right' : 'left',
    },
    '&:focus-within:after': {
      transform: 'scaleX(1)',
    },
    //  todo: errpr state
    '&:before': {
      borderBottom: `1px solid ${themeGet('colors.sjAsh')(props)}`,
      left: 0,
      bottom: 0,
      borderBottomStyle: props.disabled ? 'dotted': 'normal',
      // Doing the other way around crash on IE 11 "''" https://github.com/cssinjs/jss/issues/242
      content: '"\\00a0"',
      position: 'absolute',
      right: 0,
      transition: `border-bottom-color ${themeGet('duration.fast')(props)}`,
      pointerEvents: 'none', // Transparent to the hover style.
    },
    '&:hover:before': {
      borderBottom: `2px solid ${themeGet('colors.sjCarbon')(props)}`,
      // Reset on touch devices, it doesn't add specificity
      '@media (hover: none)': {
        borderBottom: `1px solid ${themeGet('colors.sjCarbon')(props)}`,
      },
    },
  } as CSSObject;
}





const StyledInputBase = styled(InputBase)<InputProps>`
  position: relative;

  ${formControl}
  ${underline}
  ${space}
`;

const Input = React.forwardRef(function Input(props: InputProps, ref: React.Ref<HTMLDivElement>) {
  const {
    disableUnderline,
    fullWidth = false,
    inputComponent = 'input',
    multiline = false,
    type = 'text',
    as, // todo: think of something for as prop, as typescript warns for incompatibility if not discluded like this
    ...other
  } = props;

  return (
    <StyledInputBase
      // classes={{
      //   ...classes,
      //   root: clsx(classes.root, {
      //     [classes.underline]: !disableUnderline,
      //   }),
      //   underline: null,
      // }}
      fullWidth={fullWidth}
      inputComponent={inputComponent}
      multiline={multiline}
      ref={ref}
      type={type}
      {...other}
    />
  );
});

Input.displayName = 'Input';


Input.defaultProps = {
  theme: defaultTheme,
};

export default Input;
