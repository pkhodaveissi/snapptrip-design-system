import React from 'react';
import styled from 'styled-components';
import theme from '../theme';
import Icon from '../Icon';
import Button, {ButtonProps} from '../Button';
import Text from '../Text';

interface IconButtonProps extends ButtonProps {
  name?: string,
  iconSize: number,
  text?: string,
}

const TransparentButton = styled(Button)<ButtonProps>`
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  justify-content: center;
`;

const IconButton = ({ name, iconSize, text, ...props }: IconButtonProps) => (
  <TransparentButton size={!text ? 'icon' : undefined} {...props}>
    <Icon name={name} size={iconSize} title={name} />
    {text ? (
      <Text medium fontSize={iconSize / 2} mr={8}>
        {text}
      </Text>
    ) : null}
  </TransparentButton>
);

IconButton.displayName = 'IconButton';

// IconButton.propTypes = {
//   name: PropTypes.string,
//   text: PropTypes.string,
//   colors: PropTypes.string,
//   size: PropTypes.number,
//   children: PropTypes.oneOfType([
//     PropTypes.arrayOf(PropTypes.node),
//     PropTypes.node,
//   ]),
//   theme: PropTypes.object,
// };

IconButton.defaultProps = {
  theme,
  colors: 'carbonAndWhite',
  iconSize: 24,
};

export default IconButton;
