import styled from 'styled-components'
import {
  space,
  width,
  color,
  alignItems,
  justifyContent,
  flexWrap,
  flexDirection,
  SpaceProps,
  WidthProps,
  ColorProps,
  AlignItemsProps,
  JustifyContentProps,
  FlexWrapProps,
  FlexDirectionProps
} from 'styled-system'
import { mapProps } from '../utils';
import theme from '../theme'

export interface FlexProps
  extends SpaceProps,
    WidthProps,
    ColorProps,
    AlignItemsProps,
    JustifyContentProps,
    FlexWrapProps,
    FlexDirectionProps{
      wrap?: boolean,
      theme?: typeof theme,
    }

// const Flex = styled.div<FlexProps>`
//   display: flex;
//   ${space} 
//   ${width}
//   ${color} 
//   ${alignItems} 
//   ${justifyContent}
//   ${flexDirection}
//   ${flexWrap}
// `;
const Flex = mapProps<FlexProps>(({ wrap, align, justify, ...props }) => ({
  flexWrap: wrap ? 'wrap' : undefined,
  ...props,
}))(styled.div`
  display: flex;
  ${space}
  ${width}
  ${color}
  ${alignItems}
  ${justifyContent}
  ${flexDirection}
  ${flexWrap}
`);
Flex.defaultProps = {
  theme
}


Flex.displayName = 'Flex'

export default Flex
