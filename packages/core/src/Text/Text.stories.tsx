import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { Text, Box } from '../';

storiesOf('Text', module)
  .add(
    'Typography component',
    withInfo({
      inline: true,
      text:
        'A low-level component for setting font-size, typographic styles, margin, and color',
    })(() => <Text m={3}>سلام</Text>),
  )
  .add('fontSize', () => (
    <div>
      <Text pb={3} bold fontSize={4} textAlign="left">
        Typography
      </Text>
      <Text bold as="span" px={3}>
        Bold
      </Text>
      <Text medium as="span" px={2}>
        Medium
      </Text>
      <Text as="span" px={2}>
        Regular
      </Text>
      <Text light as="span" px={2}>
        Light
      </Text>
      <Box m={3}>
        <Text bold fontSize={4}>
          سرتیتر
        </Text>
        <Text fontSize={0}>۲۲ پیکسل، صرفا برای سرتیترها</Text>
      </Box>
      <Box m={3}>
        <Text bold fontSize={3}>
          تیتر
        </Text>
        <Text fontSize={0}>۲۰ پیکسل، صرفا برای تیترها</Text>
      </Box>
      <Box m={3}>
        <Text bold fontSize={2}>
          درشت
        </Text>
        <Text fontSize={0}>۱۷ پیکسل</Text>
      </Box>
      <Box m={3}>
        <Text medium fontSize={1}>
          متوسط
        </Text>
        <Text fontSize={0}>۱‍۴ پیکسل</Text>
      </Box>
      <Box m={3}>
        <Text medium fontSize={0}>
          ریز
        </Text>
        <Text fontSize={0}>۱۲ پیکسل</Text>
      </Box>
    </div>
  ))
  .add('textAlign', () => (
    <div>
      <Text textAlign="left">سلام چپ</Text>
      <Text textAlign="center">سلام وسط</Text>
      <Text textAlign="right">سلام راست</Text>
    </div>
  ))
  .add('regular', () => <Text>سلام ساده</Text>)
  .add('bold', () => <Text bold>سلام درشت</Text>)
  .add('strikethrough', () => <Text as="s">سلام خط خورده</Text>)
  .add('margin', () => (
    <Text mt={4} mb={2}>
      سلام فاصله دار
    </Text>
  ))
  .add('color', () => (
    <div>
      <Text color="sjDarkBlue">سلام آبی</Text>
      <Text color="sjDarkGreen">سلام سبز</Text>
    </div>
  ));
