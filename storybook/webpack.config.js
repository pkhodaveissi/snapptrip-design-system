const path = require('path')

// Export a function. Accept the base config as the only param.
module.exports = async ({ config, mode }) => {
  // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
  // You can change the configuration based on that.
  // 'PRODUCTION' is used when building the static version of storybook.

  // Make whatever fine-grained changes you need
  config.resolve.alias = {
    ...config.resolve.alias,
    'styled-components': path.join(
      __dirname,
      '../node_modules/styled-components'
    ),
    'styled-system': path.join(__dirname, '../node_modules/styled-system'),
    'prop-types': path.join(__dirname, '../node_modules/prop-types'),
    '@babel/runtime': path.join(__dirname, '../node_modules/@babel/runtime'),
    'snappjek-design-system': path.join(
      __dirname,
      '../packages/core/src/index'
    ),
    'snappjek-icons': path.join(__dirname, '../packages/icons')
  }

  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    use: [
      {
        loader: require.resolve('ts-loader')
      },
      {
        loader: require.resolve('react-docgen-typescript-loader')
      }
    ]
  })
  config.resolve.extensions.push('.ts', '.tsx')

  // Return the altered config
  return config
}
