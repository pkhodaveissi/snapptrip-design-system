/*
 * This file is part of the nivo project.
 *
 * Copyright 2016-present, Raphaël Benitte.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import last from 'lodash/last'
const stackValueReducer = (colorScale, useAverage) => (acc, v1, index) => {
  const v0 = last(acc) !== undefined ? last(acc).v1 : 0
  let sequentialValue = useAverage === true ? v0 + (v1 - v0) / 2 : v1
  return [
    ...acc,
    {
      index,
      v0,
      v1,
      color: colorScale(
        colorScale.type === 'sequential' ? sequentialValue : index
      )
    }
  ]
}
const stackObjectReducer = (colorScale, useAverage) => (acc, v1, index) => {
  const v0 = last(acc) !== undefined ? last(acc).v1 : { value: 0 }
  let sequentialValue =
    useAverage === true ? v0.value + (v1.value - v0.value) / 2 : v1.value
  return [
    ...acc,
    {
      index,
      v0: v0,
      v1: v1,
      color: colorScale(
        colorScale.type === 'sequential' ? sequentialValue : index
      )
    }
  ]
}
export const stackValues = (values, colorScale, useAverage = false) => {
  const isObject = typeof values[0] === 'object'
  const normalized = [...values]
    .filter(v => (typeof v === 'object' ? v.value !== 0 : v !== 0))
    .sort((a, b) => {
      return typeof isObject ? a.value - b.value : a - b
    })
  const stackReducer = isObject
    ? stackObjectReducer(colorScale, useAverage)
    : stackValueReducer(colorScale, useAverage)
  return normalized.reduce(stackReducer, [])
}

export const getComputeRect = ({ layout, reverse, scale, height }) => {
  if (layout === 'horizontal') {
    if (reverse === true) {
      return d => {
        const x = scale(typeof d.v1 === 'object' ? d.v1.value : d.v1)
        const w = scale(typeof d.v0 === 'object' ? d.v0.value : d.v0) - x

        return { x, y: 0, width: w, height }
      }
    }

    return d => {
      const x = scale(typeof d.v0 === 'object' ? d.v0.value : d.v0)
      const w = scale(typeof d.v1 === 'object' ? d.v1.value : d.v1) - x

      return { x, y: 0, width: w, height }
    }
  }

  if (reverse === true) {
    return d => {
      const y = scale(typeof d.v0 === 'object' ? d.v0.value : d.v0)
      const h = scale(typeof d.v1 === 'object' ? d.v1.value : d.v1) - y

      return { x: 0, y, width: height, height: h }
    }
  }

  return d => {
    const y = scale(typeof d.v1 === 'object' ? d.v1.value : d.v1)
    const h = scale(typeof d.v0 === 'object' ? d.v0.value : d.v0) - y

    return { x: 0, y, width: height, height: h }
  }
}

export const rgbToHex = rgb => {
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/)

  function hexCode(i) {
    return ('0' + parseInt(i).toString(16)).slice(-2)
  }
  return '#' + hexCode(rgb[1]) + hexCode(rgb[2]) + hexCode(rgb[3])
}

export const computeRects = ({ data, layout, reverse, scale, height }) => {
  const computeRect = getComputeRect({
    layout,
    reverse,
    scale,
    height
  })

  return data.map(d => ({
    data: d,
    ...computeRect(d)
  }))
}
